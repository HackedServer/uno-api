#!/usr/bin/perl

{
    package UnoAPIweb;

#    use HTTP::Server::Simple::CGI;
    use base qw(HTTP::Server::Simple::CGI);
    use config;
    use tasks;
    use JSON;

my %dispatch = (
#    '/' => \&UnoAPI_start,
    '/api' => \&UnoAPI_start,
    '/api/' => \&UnoAPI_start,
    '/api/creategame' => \&UnoAPI_creategame,
    '/api/startgame' => \&UnoAPI_startgame,
    '/api/checkapikey' => \&UnoAPI_checkapikey,
    '/api/addplayer' => \&UnoAPI_addplayer,
    '/api/delplayer' => \&UnoAPI_delplayer,
    '/api/gameinfo' => \&UnoAPI_gameinfo,
    '/api/draw' => \&UnoAPI_draw,
);



sub handle_request {
    my $self = shift;
    my $cgi  = shift;
    my $path = $cgi->path_info();
    my $handler = $dispatch{$path};

    if (ref($handler) eq "CODE") {
        print "HTTP/1.0 200 OK\r\n";
	print $cgi->header;
        $handler->($cgi);
    } else {
        print "HTTP/1.0 404 Not found\r\n";
        print $cgi->header,
              $cgi->start_html('Not found'),
              $cgi->h1('Not found'),
              $cgi->end_html;
    }
}

sub UnoAPI_addplayer {
	my $cgi = shift;
	my %apivars = $cgi->Vars;
	return if !ref $cgi;
	print UnoAPItasks::addplayer($apivars{gid},$apivars{player});
}

sub UnoAPI_delplayer {
	my $cgi = shift;
	my %apivars = $cgi->Vars;
	return if !ref $cgi;
	print UnoAPItasks::delplayer($apivars{gid},$apivars{player});
}


sub UnoAPI_start {
        my $cgi = shift;
        my @names = $cgi->param;
        my %apivars = $cgi->Vars;
        return if !ref $cgi;

	#temp
	open FILE, "<", "readme.md";
	while (<FILE>) { print "$_<br>";}
	
        print "<br><br>" . encode_json(\%apivars);

}

sub UnoAPI_startgame {
	my $cgi = shift;
	my %apivars = $cgi->Vars;
	return if !ref $cgi;

	print UnoAPItasks::startgame($apivars{gid});

}

sub UnoAPI_creategame {
        my $cgi = shift;
        my %apivars = $cgi->Vars;
        return if !ref $cgi;

        print UnoAPItasks::creategame(%apivars);

}

sub UnoAPI_gameinfo {
	my $cgi = shift;
	my %apivars = $cgi->Vars;
	return if !ref $cgi;
	
	print UnoAPItasks::gameinfo($apivars{gid});

}

sub UnoAPI_checkapikey {
	my $cgi = shift;
        my %apivars = $cgi->Vars;

        print UnoAPItasks::checkapikey($apivars{apiKey}) ? "TRUE" : "FALSE";
}	

sub UnoAPI_draw {
	my $cgi = shift;

        print UnoAPItasks::cards();
	
}
}


