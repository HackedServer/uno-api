package UnoAPItasks;

	use game;

	my %activegames;

sub addplayer {

	my $gameid = shift;
	my $playername = shift;
	
	return $activegames{$gameid}->addplayer($playername);

}

sub startgame {

	my $gid = shift;

	return $activegames{$gid}->startgame();

}


sub delplayer {

	my $gameid = shift;
	my $playername = shift;
	
	return $activegames{$gameid}->delplayer($playername);

}

sub checkapikey {

	use DBI();
	use config;

     	my $apikey = shift;

     	my $dbh = DBI->connect("DBI:mysql:database=$UnoAPIconfig::UnoAPIconfig{'mysql'}{'database'};host=$UnoAPIconfig::UnoAPIconfig{'mysql'}{'host'};port=$UnoAPIconfig::UnoAPIconfig{'mysql'}{'port'}",$UnoAPIconfig::UnoAPIconfig{'mysql'}{'user'},$UnoAPIconfig::UnoAPIconfig{'mysql'}{'password'},{'RaiseError' => 0,'PrintError'=>0,'AutoCommit'=>1}) or print $DBI::errstr;
	my $dbr = $dbh->prepare("SELECT count(*) FROM apis WHERE apikey = ?");

	$dbr->execute($apikey) or print $DBI::errstr;
	my $sth = $dbr->fetchrow();
	$dbr->finish();
	$dbh->disconnect();

	return $sth;

}


sub creategame {

	use game;

     	my %apivars = @_;

	return 0 unless checkapikey($apivars{apiKey});
		
	my $gamenumber;
	$gamenumber .= int(rand(10)) foreach (1 .. 20);

	my $gamecode = UnoAPIgame->creategame($apivars{apiKey},$gamenumber);
	$activegames{$gamenumber} = $gamecode;
	return $gamecode->gameinfo();
}

sub gameinfo {
	use game;

	my $gameid = shift;

	return $activegames{$gameid}->gameinfo();

}

sub cards {

my @unocards = ( "b0",
	"b1",
	"b1",
	"b2",
	"b2",
	"b3",
	"b3",
	"b4",
	"b4",
	"b5",
	"b5",
	"b6",
	"b6",
	"b7",
	"b7",
	"b8",
	"b8",
	"b9",
	"b9",
	"g0",
	"g1",
	"g1",
	"g2",
	"g2",
	"g3",
	"g3",
	"g4",
	"g4",
	"g5",
	"g5",
	"g6",
	"g6",
	"g7",
	"g7",
	"g8",
	"g8",
	"g9",
	"g9",
	"r0",
	"r1",
	"r1",
	"r2",
	"r2",
	"r3",
	"r3",
	"r4",
	"r4",
	"r5",
	"r5",
	"r6",
	"r6",
	"r7",
	"r7",
	"r8",
	"r8",
	"r9",
	"r9",
	"y0",
	"y1",
	"y1",
	"y2",
	"y2",
	"y3",
	"y3",
	"y4",
	"y4",
	"y5",
	"y5",
	"y6",
	"y6",
	"y7",
	"y7",
	"y8",
	"y8",
	"y9",
	"y9",
	"bd2",
	"bd2",
	"bd2",
	"bd2",
	"yd2",
	"yd2",
	"yd2",
	"yd2",
	"gd2",
	"gd2",
	"gd2",
	"gd2",
	"rd2",
	"rd2",
	"rd2",
	"rd2",
	"rr",
	"rr",
	"rr",
	"rr",
	"br",
	"br",
	"br",
	"br",
	"yr",
	"yr",
	"yr",
	"yr",
	"gr",
	"gr",
	"gr",
	"gr",
	"gs",
	"gs",
	"gs",
	"gs",
	"rs",
	"rs",
	"rs",
	"rs",
	"ys",
	"ys",
	"ys",
	"ys",
	"bs",
	"bs",
	"bs",
	"bs",
	"w",
	"w",
	"w",
	"w",
	"w4",
	"w4",
	"w4",
	"w4", );

	return $unocards[rand @unocards];
	

}

    return 1;
